import React from "react";
import Index from "./src";

export default function App() {
  return (
    <React.Fragment>
      <Index />
    </React.Fragment>
  );
}
