import React from "react";
import Game from "./components/Snake";
import { SafeAreaView } from "react-native-safe-area-context";

const Index = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Game />
    </SafeAreaView>
  );
};

export default Index;
