import React, { useState, useEffect, useRef } from "react";
import { Audio } from "expo-av";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Text,
  Image,
  Vibration,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import AntIcon from "react-native-vector-icons/AntDesign";
import {
  GestureHandlerRootView,
  PanGestureHandler,
} from "react-native-gesture-handler";

const GAME_WIDTH = Dimensions.get("screen").width;
const GAME_HEIGHT = Dimensions.get("screen").height;
const BLOCK_SIZE = 10;
type Direction = "RIGHT" | "LEFT" | "DOWN" | "UP";

const Snake = () => {
  const [snake, setSnake] = useState(snakeInitial());

  const [direction, setDirection] = useState<Direction>("RIGHT");
  const [food, setFood] = useState(getRandomFoodPosition());
  const [gameOver, setGameOver] = useState(false);
  const [score, setScore] = useState({
    score: 25,
    highest: 0,
  });

  const timerId = useRef<NodeJS.Timer>();

  const maveInterval = () => {
    const head = { ...snake[0] };

    switch (direction) {
      case "RIGHT":
        head.x += 1;
        break;
      case "LEFT":
        head.x -= 1;
        break;
      case "DOWN":
        head.y += 1;
        break;
      case "UP":
        head.y -= 1;
        break;
    }
    const xBy = GAME_WIDTH / 10;
    const yBy = GAME_HEIGHT / 10;
    if (
      head.x < 0 ||
      head.x >= xBy ||
      head.y < 0 ||
      head.y >= yBy ||
      snake.some((segment) => segment.x === head.x && segment.y === head.y)
    ) {
      handleGameOver();
      return;
    } else {
      const newSnake = [...snake];
      newSnake.unshift(head);
      if (head.x === food.x && head.y === food.y) {
        playSound().then(() => {
          return;
        });
        setScore((pre) => {
          return {
            ...pre,
            score: pre.score + 1,
          };
        });
        setFood(getRandomFoodPosition());
      } else {
        newSnake.pop();
      }
      setSnake(newSnake);
    }
  };

  useEffect(() => {
    timerId.current = setInterval(maveInterval, 100);
    return () => clearInterval(timerId.current);
  }, [snake, direction, food]);

  useEffect(() => {
    const head = snake[0];
    const dx =
      direction === "RIGHT" ? BLOCK_SIZE : direction === "LEFT" ? -1 : 0;
    const dy = direction === "DOWN" ? BLOCK_SIZE : direction === "UP" ? -1 : 0;
    const newHead = { x: head.x + dx, y: head.y + dy };

    const xBy = GAME_WIDTH / 10 + BLOCK_SIZE - 1;
    const yBy = GAME_HEIGHT / 10 + BLOCK_SIZE - 1;

    if (
      newHead.x < 0 ||
      newHead.x >= xBy ||
      newHead.y < 0 ||
      newHead.y >= yBy
    ) {
      setTimeout(() => {
        handleGameOver();
        return;
      }, 50);

      return;
    }
    if (isCollidingWithBody(newHead, snake)) {
      handleGameOver();
      return;
    }
  }, [snake, direction]);

  useEffect(() => {
    storeScore(score.score).then(() => {
      return;
    });
    getScore().then((result) => {
      setScore((pre) => {
        return {
          ...pre,
          highest: result,
        };
      });
    });
  }, [score]);

  // <- ------------------ event handler ---------------- ->

  const isCollidingWithBody = (head: { x: number; y: number }, body: any[]) => {
    return body
      .slice(1)
      .some((block) => block.x === head.x && block.y === head.y);
  };

  const handleGameOver = () => {
    setGameOver(true);
    clearInterval(timerId.current);
    Vibration.vibrate();
  };

  const handleRestart = () => {
    setSnake(snakeInitial());
    setDirection("RIGHT");
    setFood(getRandomFoodPosition());
    setScore((pre) => {
      return {
        ...pre,
        score: 0,
      };
    });
    setGameOver(false);
  };

  const handleGesture = ({ nativeEvent }: any) => {
    const { translationX, translationY } = nativeEvent;
    let _direction: Direction;
    if (Math.abs(translationX) > Math.abs(translationY)) {
      if (translationX > 0) {
        _direction = "RIGHT";
      } else {
        _direction = "LEFT";
      }
    } else {
      if (translationY > 0) {
        _direction = "DOWN";
      } else {
        _direction = "UP";
      }
    }
    const oppositeDirection = {
      LEFT: "RIGHT",
      RIGHT: "LEFT",
      UP: "DOWN",
      DOWN: "UP",
    };
    if (_direction !== oppositeDirection[direction]) {
      setDirection(_direction);
    }
  };

  const handlePause = () => {
    if (timerId.current) {
      clearInterval(timerId.current);
      return;
    }
    timerId.current = setInterval(maveInterval, 100);
    return;
  };

  const renderBlock = (x: number, y: number, type: string) => {
    const blockStyle = [
      styles.block,
      { left: x * BLOCK_SIZE, top: y * BLOCK_SIZE },
    ];

    if (type === "SNAKE") {
      blockStyle.push(styles.snakeBlock as any);
    } else if (type === "FOOD") {
      blockStyle.push(styles.foodBlock as any);
    }
    return <View style={blockStyle} />;
  };

  return (
    <View style={styles.container}>
      <View>
        <View style={styles.topContaner}>
          <View style={styles.scoreBar}>
            <View style={styles.scoreBox}>
              <Icon name="circle" size={10} color={"#00cc00"} />
              <Text style={styles.scoreText}>{score.score}</Text>
            </View>
            <View style={styles.scoreBox}>
              <AntIcon name="staro" color={"#ebd100"} size={15} />
              <Text style={styles.scoreText}>{score.highest}</Text>
            </View>
          </View>
          <View>
            <View>
              <AntIcon
                onPress={handlePause}
                color={"#fff"}
                name="pause"
                size={25}
              />
            </View>
          </View>
        </View>
        {/* -------- game playground ------------ */}
        <View style={styles.fullView}>
          <GestureHandlerRootView style={{ flex: 1 }}>
            <PanGestureHandler onGestureEvent={handleGesture}>
              <View style={styles.gameArea}>
                {snake.map((segment, i) => (
                  <Icon
                    key={i}
                    name="square"
                    size={BLOCK_SIZE}
                    color={i === 0 ? "#b39f00" : getColor(i)}
                    style={{
                      position: "absolute",
                      left: segment.x * BLOCK_SIZE,
                      top: segment.y * BLOCK_SIZE,
                    }}
                  />
                ))}
                {renderBlock(food.x, food.y, "FOOD")}
              </View>
            </PanGestureHandler>
          </GestureHandlerRootView>
        </View>
      </View>
      {/* <- ------------------ game over ----------------- -> */}

      {gameOver && (
        <View style={styles.overlay}>
          <View style={styles.overlayCenter}>
            <View>
              <Text style={styles.overlayText}>Game Over</Text>
            </View>
            <View style={styles.scoreCenter}>
              <View style={styles.finishScoreBox}>
                <Text style={styles.overlyScoreText}>Your score</Text>
                <View style={styles.overlyPoints}>
                  <Icon name="circle" size={10} color={"#00cc00"} />
                  <Text style={styles.overlyScoreText}>{score.score}</Text>
                </View>
              </View>
              <View style={styles.finishScoreBox}>
                <Text style={styles.overlyScoreText}>Top Score</Text>
                <View style={styles.overlyPoints}>
                  <AntIcon name="staro" color={"#ebd100"} size={15} />
                  <Text style={styles.overlyScoreText}>{score.highest}</Text>
                </View>
              </View>
            </View>
            <View>
              <TouchableOpacity style={styles.button} onPress={handleRestart}>
                <Text style={styles.buttonText}>Restart</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

const storeScore = async (score: number) => {
  const lastScore: number = await getScore();
  if (lastScore < score) {
    const jsonValue = JSON.stringify(score);
    await AsyncStorage.setItem("@highest-score", jsonValue);
    return;
  }
};
const getScore = async () => {
  const value = await AsyncStorage.getItem("@highest-score");
  return value ? Number(value) : 0;
};

const getRandomFoodPosition = () => {
  return {
    x: Math.floor(Math.random() * 30),
    y: Math.floor(Math.random() * 30),
  };
};

const snakeInitial = () => {
  return [
    { x: 2, y: 2 },
    { x: 2, y: 2 },
    { x: 2, y: 2 },
    { x: 2, y: 2 },
    { x: 2, y: 2 },
  ];
};

const playSound = async () => {
  try {
    const sound = new Audio.Sound();
    await sound.loadAsync(require("../../assets/success.mp3"));
    await sound.playAsync();
  } catch (error) {
    console.error(error);
  }
};

const getColor = (index: number) => {
  let lenght = index > 9 ? `0${index}` : index > 99 ? index : `00${index}`;

  return `#ebd${lenght}`;
};

// <- ------------------- helper function ------------------- ->

/*
 <- ------------------- styles -------------------------- ->
*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: GAME_WIDTH,
    height: GAME_HEIGHT,
    backgroundColor: "#21003b",
  },

  topContaner: {
    paddingHorizontal: 15,
    paddingVertical: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#ffffff24",
  },

  fullView: {
    width: "100%",
    height: "100%",
  },
  gameArea: {
    overflow: "hidden",
    width: "100%",
    height: "100%",
    position: "relative",
  },
  block: {
    position: "absolute",
    width: BLOCK_SIZE,
    height: BLOCK_SIZE,
    backgroundColor: "#000",
  },
  snakeBlock: {
    backgroundColor: "#00cc00",
  },
  foodBlock: {
    backgroundColor: "#00cc00",
    borderRadius: 10,
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    alignItems: "center",
    justifyContent: "center",
  },
  overlayText: {
    fontSize: 30,
    color: "#fff",
    marginBottom: 20,
  },
  button: {
    padding: 10,
    backgroundColor: "#fff",
    borderRadius: 5,
    alignItems: "center",
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 20,
  },

  scoreBar: {
    flexDirection: "row",
    gap: 10,
  },

  scoreText: {
    color: "#fff",
    fontWeight: "800",
    marginLeft: 10,
    fontSize: 20,
  },
  overlyScoreText: {
    color: "#fff",
    fontWeight: "700",
    fontSize: 22,
  },
  scoreBox: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 5,
    backgroundColor: "#fffded9e",
    flexDirection: "row",
    alignItems: "center",
  },
  finishScoreBox: {
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    gap: 20,
  },
  overlyPoints: {
    alignItems: "center",
    flexDirection: "row",
    gap: 5,
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 4,
    backgroundColor: "#fffded9e",
  },
  overlayCenter: {
    backgroundColor: "#21003b",
    paddingHorizontal: 40,
    paddingVertical: 20,
    borderRadius: 12,
  },
  scoreCenter: {
    borderWidth: 1,
    borderColor: "#fffded9e",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginBottom: 15,
    gap: 10,
  },
});

export default Snake;
